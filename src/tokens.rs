use std::fmt::{self, Debug, Display, Formatter, Write};
use std::mem;

use common::{Direction, KeyInput, Kind, QueryToken};
use grammar::{Caret, Completer, Grammar, Needle, Prediction};
use self::TokenMut::*;
use sequence::{self, Sequence};


type Tokens = Sequence<TokenMut>;

pub struct Session {
    completer: Completer,
    grammar: Grammar,
    tokens: Tokens,
    /// None == BOF
    pub caret: Option<Caret>,
}

impl Session {
    pub fn new() -> Session {
        Session {
            completer: Completer::new(),
            grammar: Grammar::new(),
            tokens: Sequence::new(),
            caret: None,
        }
    }

    pub fn iter_tokens(&self) -> sequence::Iter<TokenMut> {
        self.tokens.iter()
    }

    pub fn predict(&self) -> Option<Prediction> {
        if let Some(ref caret) = self.caret {
            self.completer.predict(&self.tokens, caret)
        } else {
            None
        }
    }

    pub fn plan(&self, key: KeyInput) -> Plan {
        let planner = Planner {
            grammar: &self.grammar,
            tokens: &self.tokens,
            caret: &self.caret,
        };
        planner.plan(key)
    }

    pub fn update(&mut self, op: &TokenOp) {
        let mut caret: Option<Caret> = None;
        mem::swap(&mut caret, &mut self.caret);
        self.caret = match caret {
            Some(caret) => op.apply(&mut self.tokens, caret),
            None => op.apply_at_bof(&mut self.tokens),
        };
    }

    pub fn check_invariants(&self) {
        self.grammar.check_invariants(self.iter_tokens())
    }
}

#[derive(Debug)]
pub enum Plan {
    Act(TokenOp),
    Harsh(&'static str),
}

impl Plan {
    pub fn is_act(&self) -> bool {
        matches!(self, &Plan::Act(_))
    }
}

/// Concocts plans given session state and key input
pub struct Planner<'a> {
    grammar: &'a Grammar,
    tokens: &'a Tokens,
    caret: &'a Option<Caret>,
}

impl<'a> Planner<'a> {
    fn plan(&self, key: KeyInput) -> Plan {
        use common::QueryToken;
        use self::Plan::*;
        use self::TokenOp::*;

        let ref caret = match self.caret {
            &Some(ref caret) => caret,
            &None => return self.plan_at_bof(key)
        };

        let ref here = self.tokens[&caret.pos];
        let at_last = caret.offset == here.last_offset();
        let next_token = || self.tokens.next_token(&caret.pos);

        macro_rules! push {
            ($c:expr, $spec:expr) => {{
                let (c, spec) = ($c, $spec);
                if !at_last {
                    SplitWith(spec)
                } else if let Some((_, ref next)) = next_token() {
                    let next_kind = next.kind();
                    if self.grammar.prepends_to(next_kind, c) {
                        ForePrepend(match next_kind {
                            Kind::Space => Spec::Space(0),
                            Kind::Break => Spec::Break(0),
                            _ => {
                                assert!(c != ' ' && c != '\n');
                                Spec::Tok(c, next_kind)
                            }
                        })
                    } else {
                        Push(spec)
                    }
                } else {
                    Push(spec)
                }
            }}
        }

        match key {
            KeyInput::Push(c) => {
                Act(match here {
                    &Tok {kind, ..} => {
                        if let Some(kind) = self.grammar.adds_new_token(kind, c) {
                            let spec = Spec::Tok(c, kind);
                            if at_last {
                                Push(spec)
                            } else {
                                SplitWith(spec)
                            }
                        } else {
                            Append(Spec::Tok(c, kind))
                        }
                    }
                    &Space {..} | &Break {..} => {
                        let kind = self.grammar.start_new_token(c);
                        push!(c, Spec::Tok(c, kind))
                    }
                })
            }
            KeyInput::Space => {
                let spec = Spec::Space(0);
                Act(match here {
                    &Space {..} => Append(spec),
                    &Tok {..} | &Break {..} => push!(' ', spec),
                })
            }
            KeyInput::Return => {
                let spec = Spec::Break(0);
                Act(match here {
                    &Break {..} => Append(spec),
                    &Tok {..} | &Space {..} => push!('\n', spec),
                })
            }
            KeyInput::Bksp => {
                let spec = Spec::from_offset(here, caret.offset);
                if here.last_offset() > 0 {
                    Act(BackShorten(spec))
                } else {
                    assert_eq!(caret.offset, 0);
                    Act(BackRemove(spec))
                }
            }
            KeyInput::Del => {
                let (token, offset) = if at_last {
                    match next_token() {
                        Some((_, next)) => (next, 0),
                        None => return Harsh("already at end"),
                    }
                } else {
                    (here, caret.offset + 1)
                };

                let spec = Spec::from_offset(token, offset);
                if token.last_offset() > 0 {
                    Act(ForeShorten(spec))
                } else {
                    Act(ForeRemove(spec))
                }
            }
            KeyInput::Move(Direction::Left) => {
                if caret.offset > 0 {
                    Act(ModifyCaretOffset(-1))
                } else {
                    Act(PrevToken)
                }
            }
            KeyInput::Move(Direction::Right) => {
                if !at_last {
                    Act(ModifyCaretOffset(0))
                } else if next_token().is_none() {
                    Harsh("already at end")
                } else {
                    Act(NextToken)
                }
            }
        }
    }

    fn plan_at_bof(&self, key: KeyInput) -> Plan {
        use self::Plan::*;
        use self::TokenOp::*;

        macro_rules! push {
            ($c:expr, $spec:expr) => {{
                let (c, spec) = ($c, $spec);
                if let Some((_, ref next)) = self.tokens.first_token() {
                    let next_kind = next.kind();
                    if self.grammar.prepends_to(next_kind, c) {
                        ForePrepend(match next_kind {
                            Kind::Space => Spec::Space(0),
                            Kind::Break => Spec::Break(0),
                            _ => {
                                assert!(c != ' ' && c != '\n');
                                Spec::Tok(c, next_kind)
                            }
                        })
                    } else {
                        Push(spec)
                    }
                } else {
                    Push(spec)
                }
            }}
        }

        match key {
            KeyInput::Push(c) => {
                let kind = self.grammar.start_new_token(c);
                Act(push!(c, Spec::Tok(c, kind)))
            }
            KeyInput::Space   => Act(push!(' ', Spec::Space(0))),
            KeyInput::Return  => Act(push!('\n', Spec::Break(0))),
            KeyInput::Bksp => Harsh("already at beginning"),
            KeyInput::Del => {
                let first = match self.tokens.first_pos() {
                    Some(pos) => pos,
                    None => return Harsh("blank"),
                };
                let ref token = self.tokens[first];
                let spec = Spec::from_offset(token, 0);
                if token.last_offset() > 0 {
                    Act(ForeShorten(spec))
                } else {
                    Act(ForeRemove(spec))
                }
            }
            KeyInput::Move(Direction::Left) => Harsh("already at beginning"),
            KeyInput::Move(Direction::Right) => {
                if self.tokens.is_empty() {
                    Harsh("blank")
                } else {
                    Act(NextToken)
                }
            }
        }
    }
}

impl Spec {
    /// Remove chars before `offset` in `tokens`.
    fn back_shorten(&self, token: &mut TokenMut, offset: Needle) -> usize {
        match (token, self) {
            (&mut Tok {ref mut s, ref mut kind, ..}, &Spec::Tok(c, k)) => {
                let (ix, ch) = s.char_indices().nth(offset).expect("bshort oob");
                assert!(c == ch, "bshort: {:?} at {:?} instead of {:?}", ch, offset, c);
                s.remove(ix); // O(n)
                assert!(!s.is_empty(), "back-shortened to nothing");
                *kind = k; // clobber kind
                1 // utf8...
            }
            (&mut Space {ref mut len}, &Spec::Space(n)) => {
                assert!(n as usize <= offset, "not enough spaces before caret");
                assert!(*len > n, "not enough spaces total");
                *len -= n + 1;
                n as usize + 1
            }
            (&mut Break {ref mut count}, &Spec::Break(n)) => {
                assert!(n as usize <= offset, "not enough breaks before caret");
                assert!(*count > n, "not enough breaks total");
                *count -= n + 1;
                n as usize + 1
            }
            (ref t, _) => panic!("can't bshorten {:?} by {:?}", t, self),
        }
    }

    /// Remove chars after `offset` in `tokens`. `None` means the start of the token.
    fn fore_shorten(&self, token: &mut TokenMut, offset: Option<Needle>) -> usize {
        let index = match offset {
            Some(o) => o + 1,
            None => 0,
        };
        match (token, self) {
            (&mut Tok {ref mut s, ref mut kind, ..}, &Spec::Tok(c, k)) => {
                let (ix, ch) = s.char_indices().nth(index).expect("fshort oob");
                assert!(c == ch, "fshort: {:?} at {:?} instead of {:?}", ch, index, c);
                s.remove(ix); // O(n)
                assert!(!s.is_empty(), "fore-shortened to nothing");
                *kind = k; // clobber kind
                1 // utf8...
            }
            (&mut Space {ref mut len}, &Spec::Space(n)) => {
                assert!(index + (n as usize) <= *len as usize, "not enough spaces after caret");
                *len -= n + 1;
                n as usize + 1
            }
            (&mut Break {ref mut count}, &Spec::Break(n)) => {
                assert!(index + (n as usize) <= *count as usize, "not enough breaks after caret");
                *count -= n + 1;
                n as usize + 1
            }
            (ref t, _) => panic!("can't fshorten {:?} by {:?}", t, self),
        }
    }

    fn prepend(&self, token: &mut TokenMut) {
        match (token, self) {
            (&mut Tok {ref mut s, ref mut kind, ..}, &Spec::Tok(c, k)) => {
                s.insert(0, c);
                *kind = k; // clobber kind
            }
            (&mut Space {ref mut len}, &Spec::Space(n)) => {
                // should check overflow
                *len += n + 1;
            }
            (&mut Break {ref mut count}, &Spec::Break(n)) => {
                // should check overflow
                *count += n + 1;
            }
            (ref mut t, ref spec) => panic!("can't prepend {:?} to {:?}", spec, t),
        }
    }
}

/// One lousy token
#[derive(Clone, RustcDecodable, RustcEncodable)]
pub enum TokenMut {
    Tok { s: String, id: u32, kind: Kind },
    Space { len: u8 },
    Break { count: u8 },
}

impl TokenMut {
    pub fn last_offset(&self) -> Needle {
        match self {
            &Tok {ref s, ..} => {
                assert!(!s.is_empty());
                s.len() - 1
            }
            &Space {len} => len as Needle,
            &Break {count} => count as Needle,
        }
    }
}

impl QueryToken for TokenMut {
    fn kind(&self) -> Kind {
        match *self {
            Tok {kind, ..} => kind,
            Space {..} => Kind::Space,
            Break {..} => Kind::Break,
        }
    }

    fn len(&self) -> usize {
        match *self {
            Tok {ref s, ..} => s.len(),
            Space {len} => len as usize + 1,
            Break {count} => count as usize + 1,
        }
    }
}

impl Display for TokenMut {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            &Tok {ref s, ..} => write!(f, "{}", s),
            &Space {len: 0} => f.write_char(' '),
            &Space {len} => {
                for _ in 0..len + 1 {
                    try!(f.write_char(' '))
                }
                Ok(())
            }
            &Break {count: 0} => f.write_char('\n'),
            &Break {count} => {
                for _ in 0..count + 1 {
                    try!(f.write_char('\n'))
                }
                Ok(())
            }
        }
    }
}

impl Debug for TokenMut {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self {
            &Tok {ref s, ..} => write!(f, "`{}'", s),
            &Space {len: 0} => write!(f, " _ "),
            &Space {len} => write!(f, " _x{} ", len + 1),
            &Break {count: 0} => write!(f, ". "),
            &Break {count} => write!(f, ".x{} ", count + 1),
        }
    }
}

/// An immutable blueprint for a new token
///
/// When reading the u8s, add one for the "real" value.
/// They're intentionally bytes to discourage library users from leaning heavily on them.
#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub enum Spec {
    Tok(char, Kind),
    Space(u8),
    Break(u8),
}

impl Spec {
    fn to_mut(&self) -> TokenMut {
        // use Mutant trait?
        match self {
            &Spec::Tok(c, kind) => {
                debug_assert!(c != ' ' && c != '\n', "probably a bug");
                Tok {
                    s: c.to_string(),
                    kind: kind,
                    id: 0,
                }
            }
            &Spec::Space(n) => Space { len: n },
            &Spec::Break(n) => Break { count: n },
        }
    }

    fn from_offset(token: &TokenMut, offset: Needle) -> Self {
        assert!(offset <= token.last_offset());
        match token {
            &Tok {ref s, kind, ..} => {
                let c = s.chars().nth(offset).expect("blame unicode");
                Spec::Tok(c, kind)
            }
            &Space {..} => Spec::Space(0),
            &Break {..} => Spec::Break(0),
        }
    }

    /// Returns `self.to_mut() == token`.
    fn equals_token(&self, token: &TokenMut) -> bool {
        match (token, self) {
            (&Tok {ref s, kind, ..}, &Spec::Tok(c, k)) => {
                debug_assert_eq!(k, kind); // if these aren't equal, it's probably a bug...
                if k != kind {
                    return false
                }
                let mut chars = s.chars();
                // ensure `s` only contains the one character
                chars.next() == Some(c) && chars.next().is_none()
            }
            (&Space {len}, &Spec::Space(n)) => len == n,
            (&Break {count}, &Spec::Break(n)) => count == n,
            _ => false,
        }
    }
}

/// Represents one text-editing operation. Likely over-engineered.
///
/// TokenOps are inefficient human-level actions; when editing programmatically,
/// prefer high-level operations.
#[derive(Clone, Debug, RustcDecodable, RustcEncodable)]
pub enum TokenOp {
    Append(Spec),
    BackRemove(Spec),
    BackShorten(Spec),
    ForePrepend(Spec),
    ForeRemove(Spec),
    ForeShorten(Spec),
    /// For non-negative values, add one for the "real" offset
    /// e.g. ModifyCaretOffset(0)  => move forward one char
    ///      ModifyCaretOffset(1)  => move forward two
    ///      ModifyCaretOffset(-1) => backward one
    ModifyCaretOffset(i8),
    NextToken,
    PrevToken,
    Push(Spec),
    SplitWith(Spec),
}

impl TokenOp {
    fn apply(&self, tokens: &mut Tokens, mut caret: Caret) -> Option<Caret> {
        use tokens::TokenOp::*;

        assert!(tokens.get(&caret.pos).is_some(), "lost my caret");
        // this could probably be a nice trait method
        macro_rules! move_to_prev {
            ($pos:expr) => {{
                tokens.prev_token($pos).map(|(pos, token)| {
                    Caret {
                        pos: pos.clone(),
                        offset: token.last_offset(),
                    }
                })
            }}
        }

        match self {
            &Append(ref spec) => {
                // maybe there should just be `spec.len()` instead?
                let advance;
                match (&mut tokens[&caret.pos], spec) {
                    (&mut Tok {ref mut s, ref mut kind, ..}, &Spec::Tok(c, k)) => {
                        let off = caret.offset;
                        if off + 1 == s.len() {
                            s.push(c)
                        } else {
                            // bah, O(n)
                            let (i, _) = s.char_indices().nth(off + 1).expect("insert: oob");
                            s.insert(i, c)
                        }
                        // utf-8...
                        advance = 1;
                        *kind = k; // clobber kind
                    }
                    (&mut Space {ref mut len}, &Spec::Space(n)) => {
                        // should check overflow
                        *len += n + 1;
                        advance = n as usize + 1;
                    }
                    (&mut Break {ref mut count}, &Spec::Break(n)) => {
                        // should check overflow
                        *count += n + 1;
                        advance = n as usize + 1;
                    }
                    (ref mut t, ref spec) => panic!("can't append {:?} to {:?}", spec, t),
                }
                caret.offset += advance;
                Some(caret)
            }
            &ForePrepend(ref spec) => {
                assert_eq!(caret.offset, tokens[&caret.pos].last_offset()); // really meant it?
                let (pos, token) = tokens.next_token_mut(&caret.pos).expect("fore_prepend: eof");
                spec.prepend(token);
                Some(Caret {
                    pos: pos.clone(),
                    offset: 0,
                })
            }
            &BackRemove(ref spec) => {
                let ref pos = caret.pos;
                {
                    let ref token = tokens[pos];
                    assert!(caret.offset == 0 && token.last_offset() == 0, "did you really mean it");
                    assert!(spec.equals_token(token),
                            "brem: expected {:?}, got {:?}", spec, token);
                }
                let prev = move_to_prev!(pos);
                let _removed = tokens.remove(pos);
                prev
            }
            &ForeRemove(ref spec) => {
                assert_eq!(caret.offset, tokens[&caret.pos].last_offset()); // really meant it?
                let pos = {
                    let (pos, token) = tokens.next_token(&caret.pos).expect("fore_remove: eof");
                    assert!(spec.equals_token(token),
                            "frem: expected {:?}, found {:?}", spec, token);
                    // probably need a `hashmap.entry()`-style interface to avoid these clone()s
                    pos.clone()
                };
                let _removed = tokens.remove(&pos);
                Some(caret)
            }
            &BackShorten(ref spec) => {
                let n = spec.back_shorten(&mut tokens[&caret.pos], caret.offset);
                if caret.offset >= n {
                    caret.offset -= n;
                    Some(caret)
                } else {
                    assert_eq!(caret.offset + 1, n);
                    move_to_prev!(&caret.pos)
                }
            }
            &ForeShorten(ref spec) => {
                if caret.offset == tokens[&caret.pos].last_offset() {
                    let (_, token) = tokens.next_token_mut(&caret.pos).expect("fore_shorten: eof");
                    spec.fore_shorten(token, None);
                } else {
                    spec.fore_shorten(&mut tokens[&caret.pos], Some(caret.offset));
                }
                Some(caret)
            }
            &ModifyCaretOffset(n) => {
                if n >= 0 {
                    let delta = (n as Needle) + 1;
                    let ref here = tokens[&caret.pos];
                    assert!(caret.offset + delta <= here.last_offset(), "caret: fell right");
                    caret.offset += delta;
                } else {
                    let delta = -(n as isize) as Needle;
                    assert!(delta <= caret.offset, "caret: fell left");
                    caret.offset -= delta;
                }
                Some(caret)
            }
            &NextToken => {
                let pos = tokens.next_pos(&caret.pos).expect("next: at eof");
                Some(Caret {
                    pos: pos.clone(),
                    offset: 0,
                })
            }
            &PrevToken => move_to_prev!(&caret.pos),
            &Push(ref spec) => {
                let token = spec.to_mut();
                let offset = token.last_offset();
                let pos = tokens.append(&caret.pos, token);
                Some(Caret {
                    pos: pos,
                    offset: offset,
                })
            }
            &SplitWith(ref spec) => {
                let (front, back) = split_token(&tokens[&caret.pos], caret.offset, spec);

                let blade = spec.to_mut();
                let blade_offset = blade.last_offset();

                // append the pieces after original, then remove original
                let front_pos = tokens.append(&caret.pos, front);
                let blade_pos = tokens.append(&front_pos, blade);
                tokens.append(&blade_pos, back);
                tokens.remove(&caret.pos);

                Some(Caret {
                    pos: blade_pos,
                    offset: blade_offset,
                })
            }
        }
    }

    // It's almost like there should be the concept of a caret-less operation...
    fn apply_at_bof(&self, tokens: &mut Tokens) -> Option<Caret> {
        use tokens::TokenOp::*;

        let maybe_caret = match self {
            &Append(_) => None,
            &BackRemove(_) => None,
            &BackShorten(_) => None,
            &ForePrepend(ref spec) => {
                let first = tokens.first_pos().expect("fore_prepend: blank").clone();
                spec.prepend(&mut tokens[&first]);
                Some(Some(Caret {
                    pos: first,
                    offset: 0,
                }))
            }
            &ForeRemove(ref spec) => {
                let first = {
                    let first = tokens.first_pos().expect("fore_remove: blank");
                    assert!(spec.equals_token(&tokens[first]), "frem mismatch");
                    first.clone()
                };
                let _removed = tokens.remove(&first);
                Some(None)
            }
            &ForeShorten(ref spec) => {
                let first = tokens.first_pos().expect("fore_shorten: blank").clone();
                spec.fore_shorten(&mut tokens[&first], None);
                Some(None)
            }
            &ModifyCaretOffset(_) => None,
            &NextToken => {
                Some(Some(Caret {
                    pos: tokens.first_pos().expect("next: at eof").clone(),
                    offset: 0,
                }))
            }
            &PrevToken => None,
            &Push(ref spec) => {
                let token = spec.to_mut();
                let offset = token.last_offset();
                let pos = tokens.insert_front(token);
                // maybe the Sequence methods should generate carets...?
                Some(Some(Caret {
                    pos: pos,
                    offset: offset,
                }))
            }
            &SplitWith(_) => None,
        };

        if let Some(caret) = maybe_caret {
            caret
        } else {
            panic!("can't {:?} at bof", self);
        }
    }
}

fn split_token(token: &TokenMut, offset: usize, spec: &Spec) -> (TokenMut, TokenMut) {

    let last_offset = token.last_offset();
    let front_len = offset + 1;
    assert!(front_len <= last_offset);
    let back_len = last_offset + 1 - front_len;
    assert!(back_len > 0 && back_len <= last_offset);

    match token {
        &Tok {ref s, kind, id} => {
            if let &Spec::Tok(c, k) = spec {
                // make sure we're not doing something weird like splitting an ident with an ident
                assert!(k != kind, "split {:?} {:?} with {:?} {:?}", kind, s, k, c);
            }
            let (byte, _) = s.char_indices().nth(front_len).expect("split: oob");
            let (f, b) = s.split_at(byte);
            assert_eq!(f.len(), front_len);
            assert_eq!(b.len(), back_len);

            // TODO: caller must provide new ids and kinds

            // arbitrarily assign custody to the front token for now
            let front = Tok {
                s: f.to_string(),
                id: id,
                kind: kind,
            };
            let back = Tok {
                s: b.to_string(),
                id: 0,
                kind: kind,
            };
            (front, back)
        }
        &Space {..} => {
            assert!(!matches!(spec, &Spec::Space(_)), "split space with space?");
            (Space {len: (front_len-1) as u8}, Space {len: (back_len-1) as u8})
        }
        &Break {..} => {
            assert!(!matches!(spec, &Spec::Break(_)), "split break with break?");
            (Break {count: (front_len-1) as u8}, Break {count: (back_len-1) as u8})
        }
    }
}

#[cfg(test)]
mod test {
    use std::char;

    use super::*;
    use super::TokenOp::*;
    use common::{Direction, KeyInput, Kind};

    macro_rules! play {
        // check the current form
        (@step $s:ident == $e:expr, $($t:tt)*) => {{
            assert_eq!(format!("{}", $s.tokens), $e);
            play!(@step $s $($t)*)
        }};

        // press keys
        (@step $s:ident > $keys:expr, $($t:tt)*) => {{
            press($s, $keys);
            play!(@step $s $($t)*)
        }};

        // apply an operation
        (@step $s:ident $op:ident$(($($arg:expr),*))*, $($t:tt)*) => {{
            let operation = $op$(($($arg),*))*;
            $s.update(&operation);
            $s.check_invariants();
            play!(@step $s $($t)*)
        }};

        (@step $s:ident) => {()};

        (($session:expr) $($t:tt)*) => {{ let s: &mut Session = $session; play!(@step s $($t)*) }};
        ($($t:tt)*) => {{ let ref mut s = Session::new(); play!(@step s $($t)*) }};
    }

    fn num(digit: u32) -> TokenOp {
        let c = char::from_digit(digit, 10).expect("non-digit");
        Push(Spec::Tok(c, Kind::Number))
    }
    fn digit(digit: u32) -> TokenOp {
        let c = char::from_digit(digit, 10).expect("non-digit");
        Append(Spec::Tok(c, Kind::Number))
    }

    /// What is this? A DSL for *ants*?
    fn press(sess: &mut Session, keys: &str) {
        for c in keys.chars() {
            let key = match c {
                'a'...'z' | '[' | ']' | '|' | '+' | '-' => KeyInput::Push(c),
                ' ' => KeyInput::Space,
                '\n' => KeyInput::Return,
                'B' => KeyInput::Bksp,
                'D' => KeyInput::Del,
                'L' => KeyInput::Move(Direction::Left),
                'R' => KeyInput::Move(Direction::Right),
                _ => panic!("{:?} not bound", c)
            };
            match sess.plan(key) {
                Plan::Act(ref op) => {
                    sess.update(op);
                    sess.check_invariants();
                }
                Plan::Harsh(err) => panic!("{:?} of {:?}: {}", c, keys, err)
            }
        }
    }

    #[test]
    fn prepend() {
        play! {
            > "bLa",
            == "ab",
            > "R c",
            == "ab c",
            > "LL",
            ForePrepend(Spec::Space(0)),
            == "ab  c",
        }
    }

    #[test]
    fn insert_op_before_ident() {
        // ensure the + doesn't grab the existing 'a'
        {
            let ref mut session = Session::new();
            play! {
                (session)
                > "aL+",
                == "+a",
            }
            // they're separate tokens right?
            assert_eq!(session.tokens.len(), 2);
        }

        // try it at non-BOF too
        {
            let ref mut session = Session::new();
            play! {
                (session)
                > "a bL+",
                == "a +b",
            }
            assert_eq!(session.tokens.len(), 4);
        }
    }

    #[test]
    fn bksp() {
        play! {
            > "abLB",
            == "b",
        }

        play! {
            > "\n\nL",
            BackShorten(Spec::Break(0)),
            == "\n",
        }
    }

    #[test]
    fn del() {
        play! {
            > "abcLLD",
            == "ac",
        }
        // explicitly:
        play! {
            > "abcLL",
            ForeShorten(Spec::Tok('b', Kind::Ident)),
            == "ac",
        }
    }

    #[test]
    fn widen_space() {
        play!{
            > "a bL",
            == "a b",
            Append(Spec::Space(0)),
            == "a  b",
        }
    }

    #[test]
    fn erase_space() {
        play! {
            > "    ",
            BackShorten(Spec::Space(0)),
            BackShorten(Spec::Space(1)),
            BackRemove(Spec::Space(0)),
            == "",
        }

        play! {
            > "    ",
            ModifyCaretOffset(-2),
            ForeShorten(Spec::Space(1)),
            PrevToken,
            ForeShorten(Spec::Space(0)),
            ForeRemove(Spec::Space(0)),
            == "",
        }
    }

    #[test]
    #[should_panic]
    fn del_too_much_space() {
        play! {
            > "  L",
            ForeShorten(Spec::Space(1)),
        }
    }

    #[test]
    fn splits() {
        play! {
            > "treeLL",
            SplitWith(Spec::Break(0)),
            == "tr\nee",
        }

        play! {
            > "[  ]",
            == "[  ]",
            PrevToken,
            ModifyCaretOffset(-1),
            SplitWith(Spec::Tok('|', Kind::Op)),
            == "[ | ]",
        }

        play! {
            > "[\n\n\n\n]",
            == "[\n\n\n\n]",
            PrevToken,
            ModifyCaretOffset(-2),
            SplitWith(Spec::Tok('|', Kind::Op)),
            == "[\n\n|\n\n]",
        }
    }

    #[test]
    fn split_different_kind() {
        play! {
            num(1),
            digit(2),
            digit(3),
            digit(4),
            ModifyCaretOffset(-2),
            SplitWith(Spec::Tok('+', Kind::Op)),
            == "12+34", // need to assert kinds too...
        }
    }

    #[test]
    #[should_panic]
    fn split_homogeneous() {
        play! {
            num(1),
            digit(3),
            ModifyCaretOffset(-2),
            SplitWith(Spec::Tok('2', Kind::Number)),
        }
    }

    #[test]
    fn plan_bksp() {
        let ref mut session = Session::new();
        play! { (session)
            > "abL",
        }
        let op = session.plan(KeyInput::Bksp);
        assert!(matches!(op, Plan::Act(BackShorten(Spec::Tok('a', Kind::Ident)))),
                "got {:?} instead", op);
    }

    /// Ensure `play!` is actually checking output
    #[test]
    #[should_panic]
    fn play_sanity_check() {
        play!{ > "a", == "b", }
    }

    #[test]
    #[should_panic]
    fn fall_right() {
        play!{ NextToken, }
    }

    #[test]
    #[should_panic]
    fn bksp_char_mismatch() {
        play! {
            > "a",
            BackRemove(Spec::Tok('b', Kind::Ident)),
        }
    }

    #[test]
    #[should_panic]
    fn bksp_kind_mismatch() {
        play! {
            > "a",
            BackRemove(Spec::Tok('a', Kind::Number)),
        }
    }

    #[test]
    #[should_panic]
    fn bksp_empty_token() {
        play! {
            > "a",
            BackShorten(Spec::Tok('a', Kind::Ident)),
        }
    }
}


pub mod terminal {
    use common::Kind;
    use super::Session;
    use super::TokenMut::*;
    use rustbox::{self, Color, RustBox};

    pub trait DrawRustBox {
        /// Returns the caret position.
        ///
        /// `pos` should be position where to begin rendering, and becomes the position where
        /// rendering stopped.
        fn render(&self, &RustBox, pos: &mut (u32, u32)) -> (u32, u32);
    }

    impl DrawRustBox for Session {
        fn render(&self, rustbox: &RustBox, pos: &mut (u32, u32)) -> (u32, u32) {
            let mut x: u32 = pos.0;
            let mut y: u32 = pos.1;
            let left_edge = x;

            let mut prev_was_ident = false;

            macro_rules! print_token {
                ($token:expr) => {{
                    match $token {
                        &Tok {ref s, kind, ..} => {
                            let mut bg = Color::Default;
                            let fg = match kind {
                                Kind::Number => Color::Blue,
                                Kind::Op => Color::Green,
                                Kind::Ident => Color::Black,
                                Kind::Unknown => Color::Red,
                                Kind::Space | Kind::Break => {
                                    // won't linebreaks be invisible even if bg is Red?
                                    bg = Color::Red;
                                    Color::White
                                }
                            };

                            // distinguish consecutive idents
                            if prev_was_ident {
                                if kind == Kind::Ident {
                                    bg = Color::Cyan;
                                }
                                prev_was_ident = false;
                            } else if kind == Kind::Ident {
                                prev_was_ident = true;
                            }

                            rustbox.print(x as usize, y as usize, rustbox::RB_NORMAL, fg, bg, s);
                            x += s.len() as u32;
                        }
                        &Space {len} => {
                            x += len as u32 + 1;
                            prev_was_ident = false;
                        }
                        &Break {count} => {
                            x = left_edge;
                            y += count as u32 + 1;
                            prev_was_ident = false;
                        }
                    }
                }}
            };

            let caret_pos;
            let mut iter = self.iter_tokens();

            if let Some(ref caret) = self.caret {
                // loop until we meet the token with the caret on it
                loop {
                    let (pos, token) = match iter.next() {
                        Some(pair) => pair,
                        None => panic!("lost caret")
                    };

                    let here = (x, y);
                    print_token!(token);

                    if caret.pos == *pos {
                        let (mut caret_x, mut caret_y) = (here.0 as isize, here.1 as isize);
                        let off = caret.offset as isize + 1;
                        match token {
                            &Tok {..}   => caret_x += off,
                            &Space {..} => caret_x += off,
                            &Break {..} => {
                                caret_x = left_edge as isize;
                                caret_y += off;
                            }
                        }
                        rustbox.set_cursor(caret_x, caret_y);
                        caret_pos = here;
                        break
                    }
                }
            } else {
                // caret at bof
                caret_pos = (x as u32, y as u32);
                rustbox.set_cursor(x as isize, y as isize);
            }

            while let Some((_, token)) = iter.next() {
                print_token!(token)
            }

            // write back
            pos.0 = x;
            pos.1 = y;

            caret_pos
        }
    }
}
