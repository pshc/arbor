use std::fmt::{self, Debug, Formatter};
use common::{Kind, QueryToken};
use common::Kind::*;
use sequence::{Pos, Sequence};

pub enum Prediction {
    Next(Kind, Needle),
}

pub struct Completer;

impl Completer {
    pub fn new() -> Self {
        Completer
    }

    pub fn predict<T>(&self, tokens: &Sequence<T>, caret: &Caret) -> Option<Prediction>
        where T: QueryToken
    {
        let ref token = tokens[&caret.pos];

        let at_completion_point = {
            // are we at the end of a line?
            if let Some((_, ref next)) = tokens.next_token(&caret.pos) {
                match next.kind() {
                    Unknown | Break => true,
                    _ => false
                }
            } else {
                true // at eof
            }
        };

        if !at_completion_point {
            return None
        }

        let before_len = token.len();
        match token.kind() {
            Unknown | Break => None,
            Space => {
                // space before us; is there a token before the space?
                if let Some((_, ref prev)) = tokens.prev_token(&caret.pos) {
                    self.predict_token_after(prev).map(|k| Prediction::Next(k, before_len))
                } else {
                    None
                }
            }
            Number | Ident => None,
            Op => {
                // just guess that a number is up next
                Some(Prediction::Next(Number, 2))
            }
        }
    }

    fn predict_token_after<T: QueryToken>(&self, token: T) -> Option<Kind> {
        match token.kind() {
            Number | Ident => Some(Op),
            Op => Some(Number),
            _ => None
        }
    }
}

pub struct Grammar;

impl Grammar {
    pub fn new() -> Self {
        Grammar
    }

    /// If a new token should be started, returns its kind.
    pub fn adds_new_token(&self, kind: Kind, c: char) -> Option<Kind> {
        match c {
            '+' | '-' => Some(Op),
            'a'...'z' | 'A'...'Z' | '0'...'9' | '_' if kind == Ident => None,
            '0'...'9' if kind == Number => None,
            c if kind == Op => Some(self.start_new_token(c)),
            _ if kind == Unknown => None,
            _ => Some(self.start_new_token(c)),
        }
    }

    pub fn start_new_token(&self, c: char) -> Kind {
        match c {
            'a'...'z' | 'A'...'Z' | '_' => Ident,
            '0'...'9' => Number,
            '+' | '-' => Op,
            _ => Unknown,
        }
    }

    /// Returns true if `c` should be prepended the following token of given kind.
    pub fn prepends_to(&self, kind: Kind, c: char) -> bool {
        match c {
            _ if kind == Op => false,
            'a'...'z' | 'A'...'Z' | '_' if kind == Ident => true,
            '0'...'9' if kind == Number => true,
            ' ' if kind == Space => true,
            '\n' if kind == Break => true,
            _ => false,
        }
    }

    pub fn check_invariants<K, I, T>(&self, tokens: I)
        where T: QueryToken, I: Iterator<Item=(K, T)>
    {
        for (_, tok) in tokens {
            let (kind, len) = (tok.kind(), tok.len());
            assert!(len > 0, "empty token");
            assert!(kind != Kind::Op || len == 1, "multi-char operator");
        }
    }
}

#[derive(Clone, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Caret {
    pub pos: Pos,
    pub offset: Needle,
}

impl Debug for Caret {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "{:?}[{}]", self.pos, self.offset)
    }
}

/// Refers to the spaces between chars in a token.
///
/// e.g. for "ABC": A . B . C .
///                   0   1   2
///
///      abc.last_offset() would return 2.
pub type Needle = usize;
