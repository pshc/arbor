use std::collections::{BTreeMap, Bound};
use std::collections::btree_map;
use std::fmt::{self, Debug, Display, Formatter};
use std::ops::{Index, IndexMut};


/// A unique immutable ID that also provides a total ordering.
///
/// Concretely, a non-empty vector.
#[derive(Clone, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Pos {
    head: Foothold,
    tail: Vec<Foothold>,
}

type Foothold = u32;

impl Pos {
    fn new<V: Into<Vec<Foothold>>>(head: Foothold, tail: V) -> Self {
        Pos {
            head: head,
            tail: tail.into(),
        }
    }

    fn with_head(head: Foothold) -> Self {
        Pos {
            head: head,
            tail: vec![],
        }
    }
}

impl Debug for Pos {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        try!(write!(f, "{}", self.head));
        for i in self.tail.iter() {
            try!(write!(f, ".{}", i));
        }
        Ok(())
    }
}

/// An ordered map that generates appropriate `Pos` keys.
pub struct Sequence<V> {
    map: BTreeMap<Pos, V>,
}

impl<V> Sequence<V> {
    pub fn new() -> Self {
        Sequence { map: BTreeMap::new() }
    }

    pub fn iter(&self) -> Iter<V> {
        Iter { inner: self.map.iter() }
    }

    /// Returns the position of the first token in the sequence.
    pub fn first_pos(&self) -> Option<&Pos> {
        self.map.keys().next()
    }

    pub fn first_token(&self) -> Option<(&Pos, &V)> {
        self.map.iter().next()
    }

    /// Returns the token before the token at `pos`, if one exists.
    pub fn prev_token(&self, pos: &Pos) -> Option<(&Pos, &V)> {
        self.map.range(Bound::Unbounded, Bound::Excluded(pos)).last()
    }

    /// Returns the position of the token after the token at `pos`, if one exists.
    pub fn next_pos(&self, pos: &Pos) -> Option<&Pos> {
        self.next_token(pos).map(first)
    }

    /// Returns the token after the token at `pos`, if one exists.
    pub fn next_token(&self, pos: &Pos) -> Option<(&Pos, &V)> {
        self.map.range(Bound::Excluded(pos), Bound::Unbounded).next()
    }

    pub fn next_token_mut(&mut self, pos: &Pos) -> Option<(&Pos, &mut V)> {
        self.map.range_mut(Bound::Excluded(pos), Bound::Unbounded).next()
    }

    /// Inserts a token just before the given position.
    pub fn insert(&mut self, caret: &Pos, token: V) -> Pos {
        // find the previous key
        let pos;
        {
            let before = self.map.range(Bound::Unbounded, Bound::Excluded(caret));
            if let Some((prev, _)) = before.last() {
                pos = Pos::between(prev, caret).unwrap_or_else(|| {
                    panic!("insert: no room between {:?} and {:?}", prev, caret)
                });
                debug_assert!(prev < &pos);
            } else {
                let ref bof = Pos::with_head(0);
                pos = Pos::between(bof, caret).unwrap_or_else(|| {
                    panic!("insert: no room before {:?}", caret)
                });
                debug_assert!(bof < &pos);
            }
            debug_assert!(&pos < caret);
        }

        assert!(self.map.insert(pos.clone(), token).is_none(),
                "pos {:?} already filled",
                pos);
        pos
    }

    /// Inserts a token just after the given position.
    pub fn append(&mut self, caret: &Pos, token: V) -> Pos {
        let pos;
        {
            let mut after = self.map.range(Bound::Excluded(caret), Bound::Unbounded);
            if let Some((next, _)) = after.next() {
                pos = Pos::between(caret, next).unwrap_or_else(|| {
                    panic!("append: no room between {:?} and {:?}", caret, next)
                });
                debug_assert!(&pos < next);
            } else {
                pos = Pos::with_head(foothold_after(Some(&caret.head)));
            }
            debug_assert!(caret < &pos);
        }

        assert!(self.map.insert(pos.clone(), token).is_none(),
                "pos {:?} already filled",
                pos);
        pos
    }

    #[allow(dead_code)]
    pub fn push_back(&mut self, token: V) -> Pos {
        // generate a new position at the end
        let pos = {
            let last_key = self.map.range(Bound::Unbounded, Bound::Unbounded).last().map(first);
            Pos::with_head(foothold_after(last_key.map(|last| &last.head)))
        };
        assert!(self.map.insert(pos.clone(), token).is_none(),
                "pos {:?} already filled",
                pos);
        pos
    }

    pub fn insert_front(&mut self, token: V) -> Pos {
        if let Some(first) = self.first_pos().cloned() {
            self.insert(&first, token)
        } else {
            debug_assert!(self.is_empty());
            let pos = Pos::with_head(foothold_after(None));
            assert!(self.map.insert(pos.clone(), token).is_none(),
                    "pos {:?} already filled",
                    pos);
            pos
        }
    }

    pub fn remove(&mut self, pos: &Pos) -> Option<V> {
        self.map.remove(pos)
    }

    #[allow(dead_code)]
    pub fn len(&self) -> usize {
        self.map.len()
    }

    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub fn get(&self, index: &Pos) -> Option<&V> {
        self.map.get(index)
    }
}

/// haskell pls
fn first<A, B>(pair: (A, B)) -> A {
    pair.0
}

impl<'a, V> Index<&'a Pos> for Sequence<V> {
    type Output = V;
    fn index(&self, index: &Pos) -> &V {
        &self.map[index]
    }
}

impl<'a, V> IndexMut<&'a Pos> for Sequence<V> {
    fn index_mut(&mut self, index: &Pos) -> &mut V {
        self.map.get_mut(index).unwrap_or_else(|| panic!("{:?} not present", index))
    }
}

impl<V: Debug> Debug for Sequence<V> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        try!(write!(f, "["));
        let mut first = true;
        for token in self.map.values() {
            if first {
                try!(write!(f, "{:?}", token));
                first = false;
            } else {
                try!(write!(f, " {:?}", token));
            }
        }
        write!(f, "]")
    }
}

#[derive(Clone)]
pub struct Iter<'a, V: 'a> {
    inner: btree_map::Iter<'a, Pos, V>
}

impl<'a, V> Iterator for Iter<'a, V> {
    type Item = (&'a Pos, &'a V);

    fn next(&mut self) -> Option<Self::Item> { self.inner.next() }
    fn size_hint(&self) -> (usize, Option<usize>) { self.inner.size_hint() }
}

impl<'a, V> DoubleEndedIterator for Iter<'a, V> {
    fn next_back(&mut self) -> Option<(&'a Pos, &'a V)> { self.inner.next_back() }
}


// this is coupled to our TokensMut implementation;
// it really should be implemented in a newtype.
impl<V: Display> Display for Sequence<V> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        for token in self.map.values() {
            try!(Display::fmt(token, f))
        }
        Ok(())
    }
}

/// Qualifies a closed interval.
#[derive(Debug, Eq, PartialEq)]
enum Gap {
    /// There is space; suggested pivot
    Spacious(Foothold),
    /// The two are adjacent
    Flush,
    /// They're equal
    Zero,
    /// You cray
    Negative,
}

impl Gap {
    fn between(a: Foothold, b: Foothold) -> Self {
        use sequence::Gap::*;

        let breathing_room = 10;

        if a == b {
            Zero
        } else if a > b {
            Negative
        } else if b - a == 1 {
            Flush
        } else if b - a < breathing_room * 2 {
            // have a narrow space? take the midpoint
            Spacious(a + (b - a) / 2)
        } else {
            Spacious(a + breathing_room)
        }
    }
}

impl Pos {
    /// Generates a fresh position between the two given positions ("fenceposts").
    ///
    /// This should always succeed unless the fenceposts are equal or have no gap between the two.
    /// (For example, these are bad arguments: (1, 1) (4.0, 4.0.0)
    /// In light of this, `Pos::between` always tries to leave gaps between fenceposts and result.
    fn between(pre: &Pos, post: &Pos) -> Option<Pos> {
        use sequence::Gap::*;

        let pos = match Gap::between(pre.head, post.head) {
            Spacious(head) => Pos::with_head(head),
            Flush => Pos::new(pre.head, vec![foothold_after(pre.tail.first())]),
            Zero => {
                if let Some(tail) = tail_between(&pre.tail[..], &post.tail[..]) {
                    Pos::new(pre.head, tail)
                } else {
                    return None
                }
            }
            Negative => return None
        };

        debug_assert!(pre < &pos, "{:?} is not after {:?}", pos, pre);
        debug_assert!(&pos < post, "{:?} is not before {:?}", pos, post);
        Some(pos)
    }
}

fn foothold_after(foothold: Option<&Foothold>) -> Foothold {
    let lower_limit = *foothold.unwrap_or(&Foothold::min_value());
    if let Gap::Spacious(pivot) = Gap::between(lower_limit, Foothold::max_value()) {
        pivot
    } else {
        panic!("you really went all the way to MAX_INT?")
    }
}

/// Returns a tail between pre and post (if one exists)
fn tail_between(pre: &[Foothold], post: &[Foothold]) -> Option<Vec<Foothold>> {
    use sequence::Gap::*;

    let pre_steps = pre.len();
    let post_steps = post.len();

    // drive a wedge between pre and post
    for step in 0..pre_steps {

        if step == post_steps {
            debug_assert!(pre > post);
            return None
        }

        return Some(match Gap::between(pre[step], post[step]) {
            Spacious(pivot) => {
                let mut tail = Vec::with_capacity(step + 1);
                tail.push_all(&pre[..step]);
                tail.push(pivot);
                debug_assert_eq!(tail.len(), step + 1);
                tail
            }
            Flush => {
                // generate an intermediate point by adding a step to `pre`
                // e.g. tail_between(0.1,      0.2) == 0.1.10
                //      tail_between(0.1.50,   0.2) == 0.1.60
                //      tail_between(0.1.50.0, 0.2) == 0.1.60
                let intermediate = foothold_after(pre.get(step + 1));

                let mut tail = Vec::with_capacity(step + 2);
                tail.push_all(&pre[..step + 1]);
                tail.push(intermediate);

                debug_assert_eq!(tail.len(), step + 2);
                tail
            }
            Zero => continue,
            Negative => return None,
        })
    }

    // if we get here, pre must be a prefix of post
    debug_assert_eq!(pre, &post[0..pre_steps]);

    for step in pre_steps..post_steps {
        let zero = Foothold::min_value();
        return Some(match Gap::between(zero, post[step]) {
            Spacious(pivot) => {
                let mut tail = Vec::with_capacity(step + 1);
                tail.push_all(&post[..step]);
                tail.push(pivot);
                debug_assert_eq!(tail.len(), step + 1);
                tail
            }
            Flush => {
                let mut tail = Vec::with_capacity(step + 2);
                tail.push_all(&post[..step]);
                tail.push(zero);
                // we *could* just return `tail` at this point,
                // but let's leave some breathing room by adding an extra step
                tail.push(foothold_after(None));
                debug_assert_eq!(tail.len(), step + 2);
                tail
            }
            Zero => continue,
            Negative => return None,
        })
    }

    // at this point, post must be pre + any number of zeroes
    debug_assert!(post[pre_steps..].iter().all(|&n| n == 0));

    if pre_steps + 1 < post_steps {
        // in the case of (x, x.0.0*), grudgingly return x.0
        Some(post[..pre_steps + 1].to_vec())
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use std::str::FromStr;

    use super::*;
    use super::{Foothold, Gap};

    fn pos(s: &str) -> Pos {
        let mut steps = s.split('.');
        let head = Foothold::from_str(steps.next().unwrap()).unwrap();
        let tail: Vec<Foothold> = steps.map(|s| Foothold::from_str(s).unwrap()).collect();
        Pos::new(head, tail)
    }

    #[test]
    fn sequencing() {
        let ref mut tokens = Sequence::<&'static str>::new();
        macro_rules! check {
            ($expected:expr) => (assert_eq!($expected, &format!("{}", tokens)))
        }
        check!("");
        let ref a = tokens.insert_front("a");
        check!("a");
        let ref c = tokens.push_back("c");
        check!("ac");
        let ref b = tokens.insert(c, "b");
        check!("abc");
        let ref b2 = tokens.insert(c, "B2");
        check!("abB2c");
        let ref a2 = tokens.append(a, "A2");
        check!("aA2bB2c");

        assert!(!tokens.is_empty());
        tokens.remove(a);
        check!("A2bB2c");
        tokens.remove(b2);
        check!("A2bc");
        tokens.remove(a2);
        check!("bc");
        tokens.remove(c);
        check!("b");
        tokens.remove(b);
        assert!(tokens.is_empty());
    }

    #[test]
    fn insert_front() {
        let ref mut tokens = Sequence::new();
        for i in 0..10 {
            tokens.insert_front(i);
        }
        assert_eq!("9876543210", format!("{}", tokens));
    }

    #[test]
    fn append() {
        let ref mut seq = Sequence::new();
        let start = seq.insert_front("a");
        assert_eq!(start, pos("10"));

        let mut end = start.clone();
        for expected in &["20", "30", "40"] {
            end = seq.append(&end, ".");
            assert_eq!(end, pos(expected));
        }

        for expected in &["15", "12", "11", "10.10"] {
            assert_eq!(seq.append(&start, "h"), pos(expected));
        }
    }

    #[test]
    fn pos_between() {
        macro_rules! check {
            ($lt:expr, $mid:expr, $gt:expr) => {
                assert_eq!(Pos::between(&pos($lt), &pos($gt)), Some(pos($mid)))
            }
        }
        check!("0", "0.0", "0.0.0");
        check!("1", "1.0", "1.0.0.0");
        check!("1.0", "1.0.0", "1.0.0.0");
        check!("1", "1.10", "2");
        check!("10", "15", "20");
        check!("10", "20", "30");
        check!("10", "20", "100");
        check!("0.1", "0.1.10", "0.2");
        check!("0.1", "0.2", "0.3");
        check!("0.1", "0.2", "0.3.1");
        check!("0.1.50", "0.1.60", "0.2");
        check!("0.1.50.0", "0.1.60", "0.2");
        check!("0.1.50", "0.2", "0.3");
        check!("0.1.50", "5", "10.0");

        macro_rules! none {
            ($lt:expr, $gt:expr) => {
                assert_eq!(None, Pos::between(&pos($lt), &pos($gt)))
            }
        }
        none!("0", "0");
        none!("0", "0.0");
        none!("1.0", "1.0.0");
        none!("2.0", "1.0");
        none!("2.0.0", "2.0");
        none!("3", "2");
    }

    #[test]
    fn gaps() {
        assert_eq!(Gap::between(1, 0), Gap::Negative);
        assert_eq!(Gap::between(2, 2), Gap::Zero);
        assert_eq!(Gap::between(3, 4), Gap::Flush);
        assert_eq!(Gap::between(0, 2), Gap::Spacious(1));
        assert_eq!(Gap::between(5, 20), Gap::Spacious(12));
        assert_eq!(Gap::between(10, 30), Gap::Spacious(20));
        assert_eq!(Gap::between(0, Foothold::max_value()), Gap::Spacious(10));
    }
}
