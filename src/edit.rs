use std::cell::RefCell;
use std::fmt::{self, Display, Formatter};

use common::{Direction, KeyInput};
use tokens::{Pos, TokenArena, TokenMut};

macro_rules! ops {
    ($($dt:item)*) => {$(
        #[derive(Clone, Debug, Eq, PartialEq, RustcDecodable, RustcEncodable)]
        $dt
    )*}
}

macro_rules! sub { ($($t:tt)*) => (TreeMut::Subtree(vec![$($t)*])) }


// EDIT SESSION
// {{{

/// Holds the entire editing session
pub struct Session {
    tree: TreeMut,
    tokens: TokenArena,
    cursor: Cursor,
}

impl Session {
    /// Constructs a blank session.
    pub fn new() -> Self {
        let cursor = Cursor {
            id: 0,
            ref_count: RefCell::new(0),
        };
        Session {
            tree: sub![TreeMut::I(cursor.clone())],
            tokens: TokenArena::new(),
            cursor: cursor,
        }
    }

    pub fn root<'s>(&'s self) -> Scope<'s> {
        Scope {
            tree: &self.tree,
            cursor_id: self.cursor.id,
        }
    }

    pub fn begin<'s, F, R>(&'s mut self, mut f: F) -> R
        where F: FnMut(Tx<'s>, ScopeMut<'s>) -> R
    {
        let path = self.find_my_cursor();
        let tx = Tx {
            tokens: &mut self.tokens,
            cursor_id: self.cursor.id,
        };
        let scope = ScopeMut {
            tree: &mut self.tree,
            path_segment: path.indices,
        };
        f(tx, scope)
    }

    pub fn find_my_cursor(&self) -> PathMut {
        assert!(self.cursor.is_unique(), "cursor has duplicates");
        self.root().find_cursor().expect("cursor went missing")
    }
}

/// Borrows a subtree of the document
#[derive(Debug, Eq, PartialEq)]
pub struct Scope<'s> {
    tree: &'s TreeMut,
    cursor_id: CursorId,
}

impl<'s> Scope<'s> {
    fn find_cursor(&self) -> Result<PathMut, Unmoved> {
        find_cursor(self.cursor_id, self.tree)
    }
}

/// Mutably borrows a subtree of the document
#[derive(Debug)]
pub struct ScopeMut<'s> {
    tree: &'s mut TreeMut,
    // I would prefer this to be a borrowed slice
    // but am having difficulty with the lifetimes
    path_segment: Vec<usize>,
}

/// Context for applying operations.
///
/// Constructed by the Session.
struct Tx<'s> {
    tokens: &'s mut TokenArena,
    cursor_id: CursorId,
}

impl<'s> Tx<'s> {
    pub fn perform(&mut self, op: &CursorOperation, scope: &mut ScopeMut) {
        use edit::TreeMut::*;

        let cursor_id = self.cursor_id;
        descend_mut(scope, |vec, index| {
            // check that the cursor is actually there
            match vec.get(index) {
                Some(&I(ref curs)) => assert_eq!(curs.id, cursor_id),
                Some(&ref e) => panic!("no cursor at {}: {:?}", index, e),
                None => panic!("cursor index overflow"),
            }
            // dispatch the operation!
            op.apply_to_vec(self, vec, index)
        })
    }
}

fn descend<F, R>(tree: &TreeMut, path: &[usize], f: F) -> R
    where F: Fn(&[TreeMut], usize) -> R
{
    use edit::TreeMut::*;

    match path {
        [] => panic!("your face is too close"),
        [index] => {
            if let &Subtree(ref vec) = tree {
                f(vec, index)
            } else {
                panic!("can't index into {:?}", tree)
            }
        }
        [index, tail..] => {
            // get closer
            if let &Subtree(ref vs) = tree {
                let ref sub = vs.get(index).expect("overflow during descent");
                descend(sub, tail, f)
            } else {
                panic!("can't index into {:?}", tree)
            }
        }
    }
}

fn descend_mut<F, R>(scope: &mut ScopeMut, mut f: F) -> R
    where F: FnMut(&mut Vec<TreeMut>, usize) -> R
{
    use edit::TreeMut::*;

    match &scope.path_segment[..] {
        [] => panic!("your face is too close"),
        [index] => {
            if let &mut Subtree(ref mut vec) = scope.tree {
                f(vec, index)
            } else {
                panic!("can't index into {:?}", scope.tree)
            }
        }
        [index, _tail..] => {
            // get closer
            if let &mut Subtree(ref mut vs) = scope.tree {
                let ref mut sub_tree = vs.get_mut(index).expect("overflow during descent");

                // TEMP inefficient hack; gimme slices
                let mut sub_path = scope.path_segment.clone();
                sub_path.remove(0);

                let ref mut sub_scope = ScopeMut {
                    tree: sub_tree,
                    path_segment: sub_path,
                };
                descend_mut(sub_scope, f)
            } else {
                panic!("can't index into {:?}", scope.tree)
            }
        }
    }
}


pub type CursorId = u32;

/// Represents a user's editing cursor
#[derive(Debug, Eq, PartialEq)]
struct Cursor {
    id: CursorId,
    ref_count: RefCell<i32>,
}

impl Cursor {
    fn is_unique(&self) -> bool {
        *self.ref_count.borrow() == 1
    }
}

impl Clone for Cursor {
    fn clone(&self) -> Self {
        // This is basically Rc.
        *self.ref_count.borrow_mut() += 1;
        Cursor {
            id: self.id,
            ref_count: self.ref_count.clone(),
        }
    }
}

impl Drop for Cursor {
    fn drop(&mut self) {
        *self.ref_count.borrow_mut() -= 1;
    }
}

// }}}
// DOCUMENT
// {{{

/// The immutable document
#[derive(Clone, Debug, Eq, PartialEq, RustcDecodable, RustcEncodable)]
pub enum Tree {
    // structure
    Subtree(Vec<Tree>),

    // exprs
    I(CursorId),
    Str(String),
}

/// The editable document
#[derive(Debug, Eq, PartialEq)]
enum TreeMut {
    Subtree(Vec<TreeMut>),

    I(Cursor),
    Str(Pos),
}

#[allow(dead_code)]
impl TreeMut {
    fn hole() -> Self {
        sub![]
    }

    fn is_hole(&self) -> bool {
        match self {
            &TreeMut::Subtree(ref vs) if vs.is_empty() => true,
            _ => false,
        }
    }
}

/// Turns Trees into TreeMuts.
trait Mutant: Sized {
    type Mut;
    fn as_mut<C: MutantContext>(&self, &mut C) -> Self::Mut;
}

// need a better name...
trait MutantContext {
    fn animate_cursor(&self, id: CursorId) -> Cursor;
    fn alloc_token<S: Into<String>>(&mut self, s: S) -> Pos;
}

impl<'s> MutantContext for Tx<'s> {
    fn animate_cursor(&self, id: CursorId) -> Cursor {
        // look up shared RefCell here
        panic!("don't have the context to clone {} yet", id)
    }

    fn alloc_token<S: Into<String>>(&mut self, s: S) -> Pos {
        self.tokens.push(TokenMut::Tok { s: s.into() })
    }
}

impl Mutant for Tree {
    type Mut = TreeMut;

    fn as_mut<C: MutantContext>(&self, cx: &mut C) -> TreeMut {
        match self {
            &Tree::Subtree(ref vs) => TreeMut::Subtree(vs.as_mut(cx)),
            &Tree::I(id) => TreeMut::I(cx.animate_cursor(id)),
            &Tree::Str(ref s) => TreeMut::Str(cx.alloc_token(s.clone())),
        }
    }
}

impl Mutant for Vec<Tree> {
    type Mut = Vec<TreeMut>;

    fn as_mut<C: MutantContext>(&self, cx: &mut C) -> Vec<TreeMut> {
        let mut vs = Vec::with_capacity(self.len());
        for o in self.iter() {
            vs.push(o.as_mut(cx))
        }
        vs
    }
}

// }}}
// VIS
// {{{

impl Session {
    pub fn printed_tree(&self) -> String {
        format!("{}", TreePrinter(&self.tree, &self.tokens))
    }
}

impl TreeMut {
    fn fmt_with_tokens(&self, f: &mut Formatter, tokens: &TokenArena) -> fmt::Result {
        use edit::TreeMut::*;

        match self {
            &Subtree(ref vs) if vs.is_empty() => write!(f, "X"),
            &Subtree(ref vs) => {
                try!(write!(f, "("));
                let mut first = true;
                for elem in vs.iter() {
                    if first {
                        first = false
                    } else {
                        try!(write!(f, " "))
                    }
                    try!(elem.fmt_with_tokens(f, tokens));
                }
                write!(f, ")")
            }
            &I(_) => write!(f, "I"),
            &Str(ref path) => write!(f, "{:?}", tokens[path]),
        }
    }
}

/// A silly way to bootstrap into the fmt_with_tokens context
struct TreePrinter<'a>(&'a TreeMut, &'a TokenArena);

impl<'a> Display for TreePrinter<'a> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        self.0.fmt_with_tokens(f, self.1)
    }
}

// }}}
// TREE SEARCHING
// {{{

/// Points to a particular location in the tree
#[derive(Clone, Debug)]
pub struct PathMut {
    indices: Vec<usize>,
    _point: usize,
}

fn find_cursor(id: CursorId, tree: &TreeMut) -> Result<PathMut, Unmoved> {

    fn recurse(target: CursorId, tree: &TreeMut, depth: usize) -> Option<PathMut> {
        use edit::TreeMut::*;

        match tree {
            &Subtree(ref subs) => {
                for (i, sub) in subs.iter().enumerate() {
                    if let Some(mut path) = recurse(target, sub, depth + 1) {
                        // unwind
                        path.indices.push(i);
                        return Some(path);
                    }
                }
                None
            }
            &I(ref curs) => {
                if curs.id == target {
                    Some(PathMut {
                        indices: Vec::with_capacity(depth),
                        _point: 0,
                    })
                } else {
                    None
                }
            }
            &Str(_) => None,
        }
    }

    if let Some(mut path) = recurse(id, tree, 0) {
        path.indices.reverse(); // due to bottom-up pushing
        Ok(path)
    } else {
        Err(Unmoved::LostCursor)
    }
}

// }}}
// OPERATIONS
// {{{

pub trait CursorOperation {
    /// The workhorse.
    fn apply_to_vec(&self, tx: &mut Tx, vec: &mut Vec<TreeMut>, cursor_index: usize);
}

// High-level cursor-centric node operations.
ops! {
    pub struct InsertNode(Tree);
    pub struct RemoveNode;
    pub struct MoveCursor(Direction);
}

impl CursorOperation for InsertNode {
    fn apply_to_vec(&self, tx: &mut Tx, vec: &mut Vec<TreeMut>, index: usize) {
        let to_insert = self.0.as_mut(tx);
        vec.insert(index, to_insert);
    }
}

impl CursorOperation for RemoveNode {
    fn apply_to_vec(&self, tx: &mut Tx, vec: &mut Vec<TreeMut>, index: usize) {
        use edit::TreeMut::*;

        assert!(index > 0, "already at left");
        match vec.get(index - 1).unwrap() {
            &I(_) => panic!("there's another cursor in the way!"),
            &Subtree(_) => (),
            &Str(ref pos) => {
                // remove the corresponding token too
                assert!(tx.tokens.remove(pos).is_some(), "token already gone");
            }
        }
        let _discarded = vec.remove(index - 1);
    }
}

impl CursorOperation for MoveCursor {
    fn apply_to_vec(&self, _: &mut Tx, vec: &mut Vec<TreeMut>, index: usize) {
        match self.0 {
            Direction::Left => {
                assert!(index != 0, "can't go any further left");
                vec.swap(index - 1, index);
            }
            Direction::Right => {
                assert!(index < vec.len() - 1, "can't go any further right");
                vec.swap(index, index + 1);
            }
        }
    }
}

// Low-level cursor-centric character-based operations
ops! {
    struct AppendChar(char);
}

impl CursorOperation for AppendChar {
    fn apply_to_vec(&self, tx: &mut Tx, vec: &mut Vec<TreeMut>, index: usize) {
        use edit::TreeMut::*;

        assert!(index > 0, "no nodes to the left");
        match vec.get_mut(index - 1).unwrap() {
            &mut Str(ref token_id) => {
                let c = self.0;
                let ref mut token = tx.tokens[token_id];
                match token {
                    &mut TokenMut::Tok {ref mut s} => s.push(c),
                    ref t => panic!("can't push character to {:?}", t),
                }
            }
            ref mut e => panic!("can't append char to {:?}", e),
        }
    }
}

/// The 'old-style' character-at-a-time editing strategy
pub struct TextEditing;

impl TextEditing {
    /// Determines the appropriate action given an editing state and a key
    pub fn plan(&self, scope: &Scope, key: KeyInput) -> Result<Box<CursorOperation>, Unmoved> {
        use common::KeyInput::*;
        use edit::TreeMut::*;

        let cursor_path = try!(scope.find_cursor());

        // try to figure out a contextual action
        let act = descend(scope.tree, &cursor_path.indices[..], |vec, index| {
            if index == 0 {
                return None;
            }
            // examine node to left of cursor
            match vec.get(index - 1).expect("cursor OOB") {
                &Subtree(_) => (),
                &I(ref _curs) => (),
                &Str(_) => {
                    if let Push(c) = key {
                        return Some(box AppendChar(c));
                    }
                }
            }
            None
        });
        if let Some(op) = act {
            return Ok(op);
        }

        let op: Box<CursorOperation> = match key {
            // maybe this should be as simple as `PushChar(c)`?
            Push(c) => box InsertNode(Tree::Str(format!("{}", c))),
            Space => box InsertNode(Tree::Str(" ".into())),
            Bksp => box RemoveNode,
            Move(dir) => box MoveCursor(dir),
        };

        Ok(op)
    }
}

/// Explains why the input had no effect
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Unmoved {
    LostCursor,
}

// }}}
// TESTS
// {{{

#[cfg(test)]
mod test {
    use super::*;
    use common::Direction::*;
    use common::KeyInput::*;

    #[test]
    fn construction() {
        let mut session = Session::new();
        let strat = TextEditing;

        macro_rules! check {
            ($key:expr, $expect:expr) => {{
                let op = strat.plan(&session.root(), $key).unwrap();
                session.begin(|mut tx, mut scope| tx.perform(&*op, &mut scope));
                let printed = format!("{}", super::TreePrinter(&session.tree, &session.tokens));
                assert_eq!(printed, $expect);
            }}
        }

        check!(Push('a'), "(`a' I)");
        check!(Space, "(`a' ` ' I)");
        check!(Move(Left), "(`a' I ` ')");
        check!(Push('b'), "(`ab' I ` ')");
        check!(Bksp, "(I ` ')");
    }
}

// }}}
