use std::io::{self, ErrorKind, Write};
use std::net::TcpStream;
use std::sync::mpsc::{self, Sender};
use std::thread;

use bincode::SizeLimit;
use bincode::rustc_serialize as serialize;
use bincode::rustc_serialize::EncodingError;
use rustc_serialize::Encodable;

use common;


macro_rules! try_encode {
    ($e:expr) => {
        match $e {
            Ok(v) => v,
            Err(EncodingError::IoError(e)) => return Err(e),
            Err(EncodingError::SizeLimit) => {
                return Err(io::Error::new(ErrorKind::InvalidData, "exceeded size limit"))
            }
        }
    }
}

#[derive(Debug)]
enum Control {
    Transmit(Box<[u8]>),
    Shutdown,
}

pub struct Peer {
    tx: Sender<Control>,
}

impl Peer {
    pub fn connect() -> io::Result<Self> {

        let addr = common::LOCAL_ADDR;
        let stream = try!(TcpStream::connect(addr));
        let (tx, rx) = mpsc::channel();

        thread::spawn(move || {
            use std::net::Shutdown::Both;

            let mut output = stream;

            for msg in rx.iter() {
                match msg {
                    Control::Transmit(bytes) => {
                        if let Err(e) = output.write_all(&bytes[..]) {
                            // should send the error back
                            println!("write: {:?}", e);
                            break
                        }
                    }
                    Control::Shutdown => {
                        let _ = output.shutdown(Both);
                        break
                    }
                }
            }
        });

        Ok(Peer {
            tx: tx,
        })
    }

    pub fn send<M: Encodable>(&mut self, msg: &M) -> io::Result<()> {
        // encode the message right now
        let size_limit = SizeLimit::Bounded(common::DEFAULT_SIZE_LIMIT);
        let bytes = try_encode!(serialize::encode(&msg, size_limit));
        // and send the bytes
        if self.tx.send(Control::Transmit(bytes.into_boxed_slice())).is_ok() {
            Ok(())
        }
        else {
            Err(io::Error::new(ErrorKind::BrokenPipe, "can't send; socket is closed"))
        }
    }

    pub fn shutdown(&mut self) {
        let _ = self.tx.send(Control::Shutdown);
    }
}

impl Drop for Peer {
    fn drop(&mut self) {
        self.shutdown()
    }
}
