use std::default::Default;
use std::error::Error;
use std::io::{self, BufReader, ErrorKind};
use std::net::{TcpListener, TcpStream};
use std::thread;

use bincode::SizeLimit;
use bincode::rustc_serialize as serialize;
use bincode::rustc_serialize::DecodingError;
use rustbox::{self, Color, RustBox};

use common;
use tokens::{Plan, Session};
use tokens::terminal::DrawRustBox;


pub struct Service;

impl Service {
    pub fn new() -> Self {
        Service
    }

    pub fn serve(&mut self) -> io::Result<()> {

        let addr = common::LOCAL_ADDR;
        let listener = try!(TcpListener::bind(addr));
        println!("Listening on {}.", addr);

        let mut failures = 0;
        let mut too_much = |e: &io::Error| -> bool {
            println!("service: {:?}", e);
            failures += 1;
            let give_up = failures > 10;
            if give_up {
                println!("service: too many listen errors");
            }
            give_up
        };

        macro_rules! loop_fail {
            ($e:ident) => {{
                if too_much(&$e) {
                    return Err($e)
                } else {
                    continue
                }
            }}
        }

        for conn in listener.incoming() {
            // accept one connection
            let stream = match conn {
                Ok(stream) => stream,
                Err(e) => loop_fail!(e)
            };

            let name = match stream.peer_addr() {
                Ok(addr) => format!("peer {}", addr),
                Err(e) => {
                    println!("service: peer info: {:?}", e);
                    "peer <unknown>".into()
                }
            };
            println!("New {}", name);
            // make a thread for it
            let builder = thread::Builder::new().name(name);
            if let Err(e) = builder.spawn(move || handle_peer(stream)) {
                loop_fail!(e)
            }
        }

        Ok(())
    }
}

fn handle_peer(stream: TcpStream) {
    if let Err(e) = spectate(stream) {
        if e.kind() == ErrorKind::ConnectionAborted {
            println!("Peer left");
        }
        else {
            println!("peer: {:?}", e);
        }
    }
}

macro_rules! try_decode {
    ($e:expr) => {
        match $e {
            Ok(v) => v,
            Err(DecodingError::IoError(e)) => return Err(e),
            Err(e@DecodingError::InvalidEncoding(_)) => {
                use std::error::Error;
                // ugh why string
                let kind = if e.description().contains("EOF") {
                    ErrorKind::ConnectionAborted
                } else {
                    ErrorKind::InvalidData
                };
                return Err(io::Error::new(kind, e))
            }
            Err(e@DecodingError::SizeLimit) => {
                return Err(io::Error::new(ErrorKind::InvalidData, e))
            }
        }
    }
}

fn spectate(stream: TcpStream) -> io::Result<()> {
    let rustbox = RustBox::init(Default::default()).unwrap();

    let mut stream = BufReader::new(stream);
    let size_limit = SizeLimit::Bounded(common::DEFAULT_SIZE_LIMIT);

    // welp
    let mut session = Session::new();

    let left_margin = 1u32;
    let style = rustbox::RB_NORMAL;
    let bg = Color::Default;
    rustbox.clear();
    rustbox.print(left_margin as usize, 1, style, Color::Green, bg, "Connected.");
    rustbox.present();

    loop {
        // wait for key input
        let key: common::KeyInput = try_decode!(serialize::decode_from(&mut stream, size_limit));

        // figure out what to do
        let plan = session.plan(key);
        // just... DO IT!
        if let Plan::Act(ref op) = plan {
            session.update(op);
            session.check_invariants();
        }

        rustbox.clear();

        let ref mut pos = (left_margin, 1);
        // we are reeling
        session.render(&rustbox, pos);
        pos.1 += 2;
        pos.0 = left_margin;

        rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::White, bg,
                      &format!("{:?}", session.caret));
        pos.1 += 1;

        match plan {
            Plan::Act(ref op) => {
                let desc = format!("{:?}", op);
                rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::White, bg, &desc);
            }
            Plan::Harsh(ref e) => {
                rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::Red, bg, e);
            }
        }

        rustbox.present();
    }
}
