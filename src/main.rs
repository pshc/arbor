#![feature(box_syntax, btree_range, catch_panic, collections_bound, slice_patterns, vec_push_all)]

extern crate bincode;
#[macro_use]
extern crate matches;
extern crate rustbox;
extern crate rustc_serialize;

use std::env;
use std::io::{self, Write};
use std::thread;

use common::{KeyInput, Kind};
use grammar::Prediction;
use peer::Peer;
use rustbox::{Color, RustBox};
use tokens::{Plan, Session};
use tokens::terminal::DrawRustBox;

mod common;
mod grammar;
mod mirror;
mod peer;
mod sequence;
mod tokens;

fn main() {
    let mut args = env::args();
    let exe = args.next();
    match args.next() {
        Some(ref s) if s == "--mirror" => {
            let mut mirror = mirror::Service::new();
            return mirror.serve().unwrap()
        }
        Some(ref s) => {
            println!("Unknown argument `{}`", s);
            println!("Usage: {} [--mirror]", exe.unwrap());
            return
        }
        None => (),
    }

    if let Err(any) = thread::catch_panic(|| repl().unwrap()) {
        if let Some(s) = any.downcast_ref::<&'static str>() {
            let _ = writeln!(io::stderr(), "panic: {}", s);
        } else if let Some(s) = any.downcast_ref::<String>() {
            let _ = writeln!(io::stderr(), "panic: {}", s);
        } else {
            let _ = writeln!(io::stderr(), "panic: <unknown error type, sorry>");
        }
    }
}

fn repl() -> io::Result<()> {
    let rustbox = RustBox::init(Default::default()).unwrap();

    let mut session = Session::new();

    let mut peer = match Peer::connect() {
        Ok(peer) => {
            rustbox.print(1, 1, rustbox::RB_NORMAL, Color::Green, Color::Default, "Connected.");
            Some(peer)
        }
        Err(_) => {
            rustbox.print(1, 1, rustbox::RB_BOLD, Color::Black, Color::Default, "No connection.");
            None
        }
    };

    let mut last_cursor_pos = (1, 1);
    let mut last_key_pos = (1, 4);

    loop {
        // generate prediction(s) about possible next actions
        // based on what is possible in the current state
        if let Some(prediction) = session.predict() {
            let mut pred_pos = (last_cursor_pos.0 as usize, last_cursor_pos.1 as usize);
            let fg = Color::White;
            let bg = Color::Default;
            let ref desc = match prediction {
                Prediction::Next(kind, offset) => {
                    pred_pos.0 += offset;
                    match kind {
                        Kind::Op => "+".into(),
                        Kind::Number => "0".into(),
                        kind => format!("{:?}?", kind)
                    }
                }
            };
            rustbox.print(pred_pos.0, pred_pos.1, rustbox::RB_BOLD, fg, bg, desc);
        }

        rustbox.present();

        let key = match poll_key(&rustbox, last_key_pos) {
            Some(key) => key,
            None => break
        };

        // figure out what to do
        let plan = session.plan(key);
        // just... DO IT!
        if let Plan::Act(ref op) = plan {
            session.update(op);
            session.check_invariants();
        }

        rustbox.clear();
        let style = rustbox::RB_NORMAL;
        let bg = Color::Default;
        let left_margin = 1;

        let ref mut pos = (left_margin, 1);
        // we are reeling
        last_cursor_pos = session.render(&rustbox, pos);
        pos.1 += 2;
        pos.0 = left_margin;

        rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::White, bg,
                      &format!("{:?}", session.caret));
        pos.1 += 2;

        // if they press a weird key, we'll describe that key in the plan line
        // so save the current co-ordinates for `poll_key`
        last_key_pos = *pos;

        match plan {
            Plan::Act(ref op) => {
                let desc = format!("{:?}", op);
                rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::White, bg, &desc);
            }
            Plan::Harsh(e) => {
                rustbox.print(pos.0 as usize, pos.1 as usize, style, Color::Red, bg, e);
            }
        }

        if plan.is_act() {
            // the others must be informed!
            let mut peer_gone = false;
            if let Some(ref mut p) = peer {
                if let Err(e) = p.send(&key) {
                    println!("Lost connection: {:?}", e);
                    peer_gone = true
                }
            }
            if peer_gone {
                peer = None
            }
        }
    }

    Ok(())
}

fn poll_key(rustbox: &RustBox, pos: (u32, u32)) -> Option<KeyInput> {
    use rustbox::{Event, Key};

    let log = |msg: &str| {
        let (fg, bg, style) = (Color::White, Color::Default, rustbox::RB_NORMAL);
        let ref padded = format!("{}                                   ", msg);
        rustbox.print(pos.0 as usize, pos.1 as usize, style, fg, bg, padded);
        rustbox.present();

    };

    loop {
        match rustbox.poll_event(false).unwrap() {
            Event::KeyEvent(Some(key)) => {
                use common::Direction::*;
                use common::KeyInput::*;
                return Some(match key {
                    Key::Backspace => Bksp,
                    Key::Char(' ') => Space,
                    Key::Char(c) => Push(c),
                    Key::Delete => Del,
                    Key::Left => Move(Left),
                    Key::Right => Move(Right),
                    Key::Enter => Return,
                    Key::Esc => return None,
                    other => {
                        log(&format!("{:?}", other));
                        continue
                    }
                })
            },
            ev => log(&format!("? {:?}", ev))
        }
    }
}
