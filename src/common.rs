pub const LOCAL_ADDR: &'static str = "127.0.0.1:2015";

/// Maximum byte length of a single message.
pub const DEFAULT_SIZE_LIMIT: u64 = 65536;

pub trait QueryToken {
    fn kind(&self) -> Kind;
    fn len(&self) -> usize;
}

impl<'a, T: QueryToken> QueryToken for &'a T {
    fn kind(&self) -> Kind { (**self).kind() }
    fn len(&self) -> usize { (**self).len() }
}

/// Token 'parts of speech'
#[derive(Clone, Copy, Debug, Eq, PartialEq, RustcDecodable, RustcEncodable)]
pub enum Kind {
    Unknown,
    Space,
    Break,
    Ident,
    Number,
    Op,
}

// INPUTS AND OUTPUTS
// {{{

/// Represents all interpretable key input
#[derive(Copy, Clone, Debug, Eq, PartialEq, RustcDecodable, RustcEncodable)]
pub enum KeyInput {
    Push(char),
    Space,
    Return,
    Bksp,
    Del,
    Move(Direction),
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, RustcDecodable, RustcEncodable)]
pub enum Direction {
    Left,
    Right,
}

// }}}
